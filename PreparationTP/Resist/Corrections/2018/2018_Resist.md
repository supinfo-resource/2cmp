# Correction sujet Resist 2018

*difficulté: \*\*\*, temps correcteur : 40min*

## Exercice 1: Analyseur (10pts)

### 1.1 : cf: [lexer.l](lexer.l)

### 1.2 : cf: [parser.y](parser.y)

## Exercice 2: Automates (5pts)

Transformation en ER:

```OCaml
S->b | SB | c
A->aAb | S
B->a
```

Points d'entrée (logique): A (sinon, le cas A est inutiles).

Cependant, selon la définition du cours, dans le cas ou le point d'entré n'est pas définis, la première ligne fait office d'axiome (donc S). Mais on peut supposer que A soit utilisé ici pour indiquer l'Axiome.

`a{n}(b|c){o}a{p}b{n}` avec `n >= 0`, `o >= 0` et `o-1 <= p <= o`

Nous utiliserons les symboles de piles suivant: `$,p1,p2`
![](images/2018_automate.png)

## Exercice 3: ER (5pts)

ER: `(vw)*(t)+ (v|f|u)+`

M1: vtfu Faux. le `t` ne peux pas être en `v` et `fu`. Il manque un `w` avant le `t` ou le `t` doit être supprimé.

M2: vwvtu Faux. le `t` ne peux pas être en `v` et `fu`. Il manque un `w` avant le `t` ou le `t` doit être supprimé.

M3: tttuu Ok

M4: vwvwvwwwwtfu Faux: `wwww` se succèdent sans `v`.

M5: vvwwvtwttbvbu Faux: L'expression ne peut pas comment par `vv`