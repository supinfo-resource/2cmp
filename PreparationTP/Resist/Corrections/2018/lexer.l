%{
    #include "parser.h"
%}
%option noyywrap
%%
a      return TA;
b      return TB;
c      return TC;
\n     /* don't like whitelines*/
[ \t]+ /* same goes for whitespace*/
.      yyerror(yytext); return ERROR;
%%