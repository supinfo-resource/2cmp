%{
    #include <stdio.h>
    int yylex();
    void yyerror(const char *str);
%}
%token TA TB TC ERROR
%%
As: A As | A;

A: TA A TB {printf("Ok");} | S {printf("ok");} | ERROR {yyerrok;};

S: TB | TC | S B;

B: TA;
%%
void yyerror(const char *str)
{
        fprintf(stderr,"lexical error: %s is not in the alphabet\n",str);
}

int main(void)
{
        int r;
        printf("Exit by typing 'exit'.\n\n");
        r = yyparse();
        printf("\n");
        return r;
} 