# 2 CMP

Compilation resources.
Contain all courses TP correction as well as training subject.

## Working environement

- Linux `sudo apt-get install gcc flex byacc`
- Mac `brew install gcc flex bison` (if exist in brew, used `byacc` instead of `bison`)

## Automata tool

[http://madebyevan.com/fsm/](http://madebyevan.com/fsm/) is the one I used. You can also use yEd or any graphics editor you like and master.

## Labs Correction

When codes have to be generated, I used `Makefile` to allow you to go through the compilations process. Think to use it (in general, you can just run `make` in the folder where the code is, and `make test` if the rules exist). 

### Part 1

| TP Number | Tp Group | File to consult               | Lines     | Link                                                                        |
| --------- | -------- | ----------------------------- | --------- | --------------------------------------------------------------------------- |
|  1        | 2        | discovery/factorielle.c       | all       | [click](discovery/factorielle.c)                                       |
|           | 3        | discovery,discovery/Makefile  | all       |                                                                             |
|  2        | 1        | Regex.md                      | 1 - 26    | [Regex.md](Regex.md)                                                   |
|           | 2        | Regex.md                      | 27 - 34   | [Regex.md](Regex.md#2---réécriture)                                    |
|           | 3        | Regex.md                      | 35 - end  | [Regex.md](Regex.md#3---signification-des-expressions)                 |
|  3        | 1        | Automates.md                  | 1 - 12    | [Automates.md](Automates.md)                                           |
|           | 2        | images/morse.png              |           | [Automates.md](Automates.md#morse)                                     |
|           | 3        | Automates.md                  | 17 - 30   | [Automates.md](Automates.md#er)                                        |
|           | 4        | Automates.md                  | 31 - end  | [Automates.md](Automates.md#automates-à-pile)                          |
| 4         | 1        | Grammaire.md                  | 3 - 70    | [Grammaire.md](Grammaire.md)                                           |
|           | 2        | Grammaire.md                  | 71 - 138  | [Grammaire.md](Grammaire.md#2---description-grammaire-de-chomsky)      |
|           | 3        | Grammaire.md                  | 139 - 171 | [Grammaire.md](Grammaire.md#3-grammaire-des-expressions-arithmétiques) |
|           | 4        | Grammaire.md                  | 172 - end | [Grammaire.md](Grammaire.md#4---grammaire-et-automates)                |
| 5         | 1        | ArbreDR.md                    | 3 - 56    | [ArbreDR.md](ArbreDR.md)                                               |
|           | 2        | ArbreDR.md                    | 57 - 79   | [ArbreDR.md](ArbreDR.md#2---expression-booleene)                       |
|           | 3        | ArbreDR.md                    | 80 - end  | [ArbreDR.md](ArbreDR.md#3---expression-arithmétique)                   |

#### Part 2

| TP Number | Tp Group | File to consult (base: lex\_yacc/tp6, lex\_ycc/tp8) | Link                                                                          |
| --------- | -------- | --------------------------------------------------- | ---------------------------------------------------------------------------   |
|  6-1      | 1        | 1/copie.l                                           | [copie.l](lex_yacc/tp6/1/copie.l)                                        |
|           | 2        | 1/compt_voyelle.l                                   | [compt_voyelle.l](lex_yacc/tp6/1/compt_voyelle.l)                        |
|           | 3        | 1/compt_consonne.l                                  | [compt_consonne.l](lex_yacc/tp6/1/compt_consonne.l)                      |
|           | 4        | 1/compt_mot.l                                       | [compt_mot.l](lex_yacc/tp6/1/compt_mot.l)                                |
|           | 5        | 1/compt_linebreak.l                                 | [compt_linebreak.l](lex_yacc/tp6/1/compt_linebreak.l)                    |
|           | 6        | 1/compt_allchars.l                                  | [compt_allchars.l](lex_yacc/tp6/1/compt_allchars.l)                      |
|           | 7        | 1/compt\_chars\_notarbreak.l                        | [compt_chars_notarbreak.l](lex_yacc/tp6/1/compt_chars_notarbreak.l)      |
|           | 8        | 1/compt_class.l                                     | [compt_class.l](lex_yacc/tp6/1/compt_class.l)                            |
|  6-2      | 1        | 2/filt\_sl\_comment.l                               | [filt_sl_comment.l](lex_yacc/tp6/2/filt_sl_comment.l)                    |
|           | 2        | 2/filt\_ml\_comment.l                               | [filt_ml_comment.l](lex_yacc/tp6/2/filt_ml_comment.l)                    |
|           | 3        | 2/filt_chars.l                                      | [filt_chars.l](lex_yacc/tp6/2/filt_chars.l)                              |
|  6-3      | 1        | 3/copie_files.l                                     | [copie_files.l](lex_yacc/tp6/3/copie_files.l)                            |
|           | 2        | 3/crypt.l                                           | [crypt.l](lex_yacc/tp6/3/crypt.l)                                        |
|           | 3        | 3/decrypt.l                                         | [decrypt.l](lex_yacc/tp6/3/decrypt.l)                                    |
|  6-4      | 1        | 4/affiche_nombre.l                                  | [affiche_nombre.l](lex_yacc/tp6/4/affiche_nombre.l)                      |
|           | 2-3-4    | 4/calculator.l                                      | [calculator.l](lex_yacc/tp6/4/calculator.l)                              |
|  6-5      |          | 5/pgm_analysor.l                                    | [pgm_analysor.l](lex_yacc/tp6/5/pgm_analysor.l)                          |
|  6-6      |          | 6/polish_syntax_checker.l                           | [polish.l](lex_yacc/tp6/6/polish_syntax_checker.l)                       |
|  6-7      |          | 7/polish_syntax_checker_automate.l                  | [polish_automate.l](lex_yacc/tp6/7/polish_syntax_checker_automate.l)     |
|  7        | 1        | Grammaire.md                                   | [Grammaire LL](Grammaire.md#1---grammaire-ll)                            |
|           | 2        | Grammaire.md                                   | [CGES](Grammaire.md#2---conception-de-grammaire--exécution-de-séquences) |
|           | 3        | Grammaire.md                                   | [Grammaire LR](Grammaire.md#3---grammaire-lr)                            |
|  8-1      | 1        | 1/calc.y                                            | [parser](lex_yacc/tp8/1/calc.y)                                          |
|           | 2        | 1/calc.l                                            | [lexer](lex_yacc/tp8/1/calc.l)                                           |
|  8-2      | 1        | 2/Grammar.md                                        | [Grammar](lex_yacc/tp8/2/Grammar.md)                                     |
|           | 2        | 2/calc.l                                            | [lexer](lex_yacc/tp8/2/calc.l)                                           |
|           | 3-5      | 2/calc.y                                            | [parser](lex_yacc/tp8/2/calc.y)                                          |
|  8-3      | 1        | 3/Grammar.md                                        | [Grammar](lex_yacc/tp8/3/Grammar.md)                                     |
|           | 1        | 3/calc.y                                            | [parser](lex_yacc/tp8/3/calc.y)                                          |
|  8-3      | 2        | 3/calc.l                                            | [lexer](lex_yacc/tp8/3/calc.l)                                           |
|  8-4      | 1        | 4/calc.reduction_errors.y                           | [parser wih erd errors](lex_yacc/tp8/4/calc.reduction_errors.y)          |
|           | 2        | 4/calc.no_reduction.y                               | [parser wih erd errors](lex_yacc/tp8/4/calc.no_reduction.y)              |
|           | 1-2      | 4/calc.l                                            | [lexer](lex_yacc/tp8/4/calc.l)                                           |
